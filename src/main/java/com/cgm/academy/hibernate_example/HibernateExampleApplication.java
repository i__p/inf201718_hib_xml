package com.cgm.academy.hibernate_example;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;
import java.util.List;

@SpringBootApplication
public class HibernateExampleApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(HibernateExampleApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        // sessionFactory tworzymy tylko raz
        // cała aplikacja korzysta z jednego obiektu
        SessionFactory sessionFactory = new Configuration()
                // domyślnie wczytany zostaje hibernate.cfg.xml
                .configure()
                .buildSessionFactory();

        // ##### To się dzieje za każdym razem gdy odwołujemy się do bazy

        // zapis danych w bazie

        // tworzymy sesję
        Session session = sessionFactory.openSession();
        session.beginTransaction(); // rozpoczęcie transakcji
        // tworzymy i zapisujemy encje
        Event event = new Event("Zapis pierwszego zdarzenia", new Date());
        session.save(event);
        Event anotherEvent = new Event("Zapis innego zdarzenia", new Date());
        session.save(anotherEvent);

        session.getTransaction().commit(); // wypchnięcie zmian
        // zakończenie sesji
        session.close();

        // wyświetlanie wyników z bazy
        // otwieramy sesję, rozpoczynamy transakcję, pobieramy dane, wyświetlamy
        // commitujemy transakcję, zamukamy sesję
        session = sessionFactory.openSession();
        session.beginTransaction();
        List result = session.createQuery("from Event").list();
        ((List<Event>) result).forEach(System.out::println);
        session.getTransaction().commit();
        session.close();

        // to można wykonać w metodzie oznaczonej @PreDestroy, albo w przypadku testów
        // możemy ubijać sessionFactory w metodzie oznaczonej jako @AfterAll
        if (sessionFactory != null) {
            sessionFactory.close();
        }
    }

}
